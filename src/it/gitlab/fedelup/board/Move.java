package it.gitlab.fedelup.board;

import it.gitlab.fedelup.exceptions.MoveFactoryInstantiationException;
import it.gitlab.fedelup.exceptions.NullMoveException;
import it.gitlab.fedelup.pieces.Pawn;
import it.gitlab.fedelup.pieces.Piece;
import it.gitlab.fedelup.pieces.Rook;

import java.util.Objects;

import static it.gitlab.fedelup.board.Board.*;
import static it.gitlab.fedelup.board.Board.Builder;
import static it.gitlab.fedelup.board.BoardUtilities.getPositionAtCoordinate;

public abstract class Move {
    private final Board board;
    private final Piece movedPiece;
    private final int destinationCoordinate;
    private boolean isFirstMove;


    public static final Move NULL_MOVE = new NullMove();


    public Move(Board board, Piece movedPiece, int destinationCoordinate) {
        this.board = board;
        this.movedPiece = movedPiece;
        this.destinationCoordinate = destinationCoordinate;
        this.isFirstMove = movedPiece.isFirstMove();
    }

    public Move(Board board, int destinationCoordinate) {
        this.board = board;
        this.movedPiece = null;
        this.destinationCoordinate = destinationCoordinate;
        this.isFirstMove = false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Move move = (Move) o;
        return destinationCoordinate == move.destinationCoordinate &&
                Objects.equals(getCurrentCoordinate(), move.getCurrentCoordinate()) &&
                Objects.equals(movedPiece, move.movedPiece);
    }

    @Override
    public int hashCode() {

        return Objects.hash(movedPiece, destinationCoordinate, movedPiece.getPosition());
    }

    protected Board getBoard() {
        return this.board;
    }

    public int getCurrentCoordinate() {
        return this.movedPiece.getPosition();
    }

    public int getDestinationCoordinate() {
        return this.destinationCoordinate;
    }

    public Piece getMovedPiece() {
        return this.movedPiece;
    }

    public boolean isAttack() {
        return false;
    }

    public boolean isCastlingMove() {
        return false;
    }

    public Piece getAttackedPiece() {
        return null;
    }

    public Board execute() {

        final Builder builder = new Builder();

        for (final Piece piece : this.getBoard().getCurrentPlayer().getActivePieces()) {
            if (!this.getMovedPiece().equals(piece)) {
                builder.setPiece(piece);
            }
        }

        for (final Piece piece : this.getBoard().getCurrentPlayer().getOpponent().getActivePieces()) {
            builder.setPiece(piece);
        }
        builder.setPiece(this.getMovedPiece().movePiece(this));
        builder.setNextPlayerMoving(this.getBoard().getCurrentPlayer().getOpponent().getPlayerSide());

        return builder.build();
    }

    public static final class NormalMove extends Move {

        public NormalMove(Board board, Piece movedPiece, int destinationCoordinate) {
            super(board, movedPiece, destinationCoordinate);
        }


        @Override
        public boolean equals(Object o) {
            return this == o || o instanceof NormalMove && super.equals(o);
        }

        @Override
        public String toString() {
            return getMovedPiece().getPieceType().toString() + getPositionAtCoordinate(this.getDestinationCoordinate());
        }
    }

    public static class MajorAttackMove extends AttackMove {

        public MajorAttackMove(Board board, Piece movedPiece, int destinationCoordinate, Piece attackedPiece) {
            super(board, movedPiece, destinationCoordinate, attackedPiece);
        }

        @Override
        public boolean equals(Object o) {
            return this == o || o instanceof MajorAttackMove && super.equals(o);
        }

        @Override
        public String toString() {
            return getMovedPiece().getPieceType() + "x" + getPositionAtCoordinate(this.getDestinationCoordinate());
        }
    }

    public static class AttackMove extends Move {

        private final Piece attackedPiece;

        public AttackMove(Board board, Piece movedPiece, int destinationCoordinate, Piece attackedPiece) {
            super(board, movedPiece, destinationCoordinate);
            this.attackedPiece = attackedPiece;
        }

        @Override
        public boolean isAttack() {
            return true;
        }

        @Override
        public Piece getAttackedPiece() {
            return this.attackedPiece;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            if (!super.equals(o)) return false;
            AttackMove that = (AttackMove) o;
            return Objects.equals(attackedPiece, that.attackedPiece);
        }

        @Override
        public int hashCode() {

            return Objects.hash(super.hashCode(), attackedPiece);
        }
    }

    public static final class PawnMove extends Move {

        public PawnMove(Board board, Piece movedPiece, int destinationCoordinate) {
            super(board, movedPiece, destinationCoordinate);
        }

        @Override
        public boolean equals(Object o) {
            return this == o || o instanceof PawnMove && super.equals(o);
        }

        @Override
        public String toString() {
            return getPositionAtCoordinate(this.getDestinationCoordinate());
        }
    }

    public static class PawnAttackMove extends AttackMove {

        public PawnAttackMove(Board board, Piece movedPiece, int destinationCoordinate, Piece attackedPiece) {
            super(board, movedPiece, destinationCoordinate, attackedPiece);
        }

        @Override
        public boolean equals(Object o) {
            return this == o || o instanceof PawnAttackMove && super.equals(o);
        }

        @Override
        public String toString() {
            return getPositionAtCoordinate(this.getMovedPiece().getPosition()).substring(0, 1) + "x" +
                    getPositionAtCoordinate(this.getDestinationCoordinate());
        }
    }

    public static final class PawnEnPassantAttackMove extends AttackMove {

        public PawnEnPassantAttackMove(Board board, Piece movedPiece, int destinationCoordinate, Piece attackedPiece) {
            super(board, movedPiece, destinationCoordinate, attackedPiece);
        }

        @Override
        public boolean equals(Object o) {
            return o == this || o instanceof PawnEnPassantAttackMove && super.equals(o);
        }

        @Override
        public Board execute() {
            final Builder builder = new Builder();
            for (Piece piece : this.getBoard().getCurrentPlayer().getActivePieces()) {
                if (!this.getMovedPiece().equals(piece)) {
                    builder.setPiece(piece);
                }
            }

            for (Piece piece : this.getBoard().getCurrentPlayer().getOpponent().getActivePieces()) {
                if (!piece.equals(this.getAttackedPiece())) {
                    builder.setPiece(piece);
                }
            }
            builder.setPiece(this.getMovedPiece().movePiece(this));
            builder.setNextPlayerMoving(this.getBoard().getCurrentPlayer().getOpponent().getPlayerSide());
            return builder.build();
        }

        @Override
        public String toString() {
            return "x" + getPositionAtCoordinate(this.getDestinationCoordinate());
        }
    }

    public static class PawnPromotion extends Move {

        private Move move;
        private Pawn promotedPawn;

        public PawnPromotion(Move move) {
            super(move.getBoard(), move.getMovedPiece(), move.getDestinationCoordinate());
            this.move = move;
            this.promotedPawn = (Pawn) move.getMovedPiece();
        }

        @Override
        public Board execute() {

            Board pawnMovedBoard = this.move.execute();
            Builder builder = new Builder();
            for (Piece piece : pawnMovedBoard.getCurrentPlayer().getActivePieces()) {
                if (!this.promotedPawn.equals(piece)) {
                    builder.setPiece(piece);
                }
            }
            for (Piece piece : pawnMovedBoard.getCurrentPlayer().getOpponent().getActivePieces()) {
                builder.setPiece(piece);
            }
            builder.setPiece(this.promotedPawn.getPromotionPiece().movePiece(this));
            builder.setNextPlayerMoving(pawnMovedBoard.getCurrentPlayer().getPlayerSide());
            return builder.build();
        }

        @Override
        public boolean isAttack() {
            return this.move.isAttack();
        }

        @Override
        public Piece getAttackedPiece() {
            return this.move.getAttackedPiece();
        }

        @Override
        public String toString() {
            return "";
        }

        @Override
        public int hashCode() {
            return Objects.hash(move, promotedPawn.hashCode());
        }

        @Override
        public boolean equals(Object o) {
            return this == o || o instanceof PawnPromotion && super.equals(o);
        }
    }

    public static final class PawnJump extends Move {

        public PawnJump(Board board, Piece movedPiece, int destinationCoordinate) {
            super(board, movedPiece, destinationCoordinate);
        }


        @Override
        public Board execute() {
            final Builder builder = new Builder();
            for (Piece piece : this.getBoard().getCurrentPlayer().getActivePieces()) {
                if (!this.getMovedPiece().equals(piece)) {
                    builder.setPiece(piece);
                }
            }
            for (Piece piece : this.getBoard().getCurrentPlayer().getOpponent().getActivePieces()) {
                builder.setPiece(piece);
            }
            Pawn movedPawn = (Pawn) this.getMovedPiece().movePiece(this);
            builder.setPiece(movedPawn);
            builder.setEnPassantPawn(movedPawn);
            builder.setNextPlayerMoving(this.getBoard().getCurrentPlayer().getOpponent().getPlayerSide());
            return builder.build();
        }

        @Override
        public String toString() {
            return getPositionAtCoordinate(this.getDestinationCoordinate());
        }
    }

    static abstract class CastleMove extends Move {

        protected Rook castleRook;
        protected int castleRookStart;
        protected int castleRookDestination;


        public CastleMove(Board board, Piece movedPiece, int destinationCoordinate, Rook castleRook, int castleRookStart, int castleRookDestination) {
            super(board, movedPiece, destinationCoordinate);
            this.castleRook = castleRook;
            this.castleRookStart = castleRookStart;
            this.castleRookDestination = castleRookDestination;
        }

        public Rook getCastleRook() {
            return this.castleRook;
        }

        @Override
        public boolean isCastlingMove() {
            return true;
        }

        @Override
        public Board execute() {

            Builder builder = new Builder();
            for (Piece piece : this.getBoard().getCurrentPlayer().getActivePieces()) {
                if (!this.getMovedPiece().equals(piece) && !this.castleRook.equals(piece)) {
                    builder.setPiece(piece);
                }
            }
            for (Piece piece : this.getBoard().getCurrentPlayer().getOpponent().getActivePieces()) {
                builder.setPiece(piece);
            }
            builder.setPiece(this.getMovedPiece().movePiece(this));
            // ...-.-.-
            builder.setPiece(new Rook(this.castleRookDestination, this.castleRook.getPiecePlayerSide()));
            builder.setNextPlayerMoving(this.getBoard().getCurrentPlayer().getOpponent().getPlayerSide());
            return builder.build();

        }

        @Override
        public int hashCode() {
            return Objects.hash(super.hashCode(), this.castleRook.hashCode(), this.castleRookDestination);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (!(o instanceof CastleMove)) {
                return false;
            }
            CastleMove otherCastleMove = (CastleMove) o;
            return super.equals(otherCastleMove) && this.castleRook.equals(otherCastleMove.getCastleRook());
        }
    }

    public static final class KingSideCastleMove extends CastleMove {

        public KingSideCastleMove(Board board, Piece movedPiece, int destinationCoordinate, Rook castleRook, int castleRookStart, int castleRookDestination) {
            super(board, movedPiece, destinationCoordinate, castleRook, castleRookStart, castleRookDestination);
        }

        @Override
        public boolean equals(Object o) {
            return this == o || o instanceof KingSideCastleMove && super.equals(o);
        }

        @Override
        public String toString() {
            return "O-O";
        }
    }

    public static final class QueenSideCastleMove extends CastleMove {

        public QueenSideCastleMove(Board board, Piece movedPiece, int destinationCoordinate, Rook castleRook, int castleRookStart, int castleRookDestination) {
            super(board, movedPiece, destinationCoordinate, castleRook, castleRookStart, castleRookDestination);
        }

        @Override
        public boolean equals(Object o) {
            return this == o || o instanceof QueenSideCastleMove && super.equals(o);
        }

        @Override
        public String toString() {
            return "O-O-O";
        }

    }

    public static final class NullMove extends Move {

        public NullMove() {
            super(null, -1);
        }

        @Override
        public Board execute() {
            throw new NullMoveException("NULL MOVE FOUND");
        }

        @Override
        public int getCurrentCoordinate() {
            return -1;
        }
    }

    public static class MoveFactory {

        private MoveFactory() {
            throw new MoveFactoryInstantiationException("Just static Methods in here");
        }

        public static Move createMove(Board board, int currentCoordinate, int destinationCoordinate) {
            for (final Move move : board.getAllLegalMoves()) {
                if (move.getCurrentCoordinate() == currentCoordinate
                        && move.getDestinationCoordinate() == destinationCoordinate) {
                    return move;
                }
            }
            return NULL_MOVE;
        }

    }


}
