package it.gitlab.fedelup.board;

import it.gitlab.fedelup.PlayerSide;
import it.gitlab.fedelup.pieces.*;
import it.gitlab.fedelup.player.BlackPlayer;
import it.gitlab.fedelup.player.Player;
import it.gitlab.fedelup.player.WhitePlayer;

import java.util.*;

import static it.gitlab.fedelup.PlayerSide.BLACK;
import static it.gitlab.fedelup.PlayerSide.WHITE;
import static it.gitlab.fedelup.board.BoardUtilities.NUM_TILES;
import static it.gitlab.fedelup.board.BoardUtilities.NUM_TILES_PER_ROW;

public class Board {

    private final List<Tile> gameBoard;
    private final Collection<Piece> whitePieces;
    private final Collection<Piece> blackPieces;

    private final WhitePlayer whitePlayer;
    private final BlackPlayer blackPlayer;
    private Player currentPlayer;

    private Pawn enPassantPawn;


    private Board(Builder builder) {
        this.gameBoard = createGameBoard(builder);
        this.whitePieces = calculateActivePieces(this.gameBoard, WHITE);
        this.blackPieces = calculateActivePieces(this.gameBoard, BLACK);
        this.enPassantPawn = builder.enPassantPawn;

        final Collection<Move> whiteStandardLegalMoves = calculateLegalMoves(this.whitePieces);
        final Collection<Move> blackStandardLegalMoves = calculateLegalMoves(this.blackPieces);

        this.whitePlayer = new WhitePlayer(this, whiteStandardLegalMoves, blackStandardLegalMoves);
        this.blackPlayer = new BlackPlayer(this, whiteStandardLegalMoves, blackStandardLegalMoves);

        this.currentPlayer = builder.nextPlayerMoving.choosePlayer(this.whitePlayer, this.blackPlayer);

    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();

        for (int i = 0; i < NUM_TILES; i++) {
            final String tileText = this.gameBoard.get(i).toString();
            builder.append(String.format("%3s", tileText));
            if ((i + 1) % NUM_TILES_PER_ROW == 0) {
                builder.append("\n");
            }
        }
        return builder.toString();

    }

    public Player getCurrentPlayer() {
        return this.currentPlayer;
    }

    public Collection<Piece> getBlackPieces() {
        return this.blackPieces;
    }

    public Collection<Piece> getWhitePieces() {
        return this.whitePieces;
    }

    private Collection<Move> calculateLegalMoves(Collection<Piece> pieces) {

        final List<Move> legalMoves = new ArrayList<>();

        for (Piece piece : pieces) {
            legalMoves.addAll(piece.calculateLegalMoves(this));
        }
        return legalMoves;
    }

    private static Collection<Piece> calculateActivePieces(List<Tile> gameBoard, PlayerSide playerSide) {
        final List<Piece> pieces = new ArrayList<>();

        for (Tile tile : gameBoard) {
            if (tile.isTileOccupied()) {
                Piece piece = tile.getPiece();
                if (piece.getPiecePlayerSide() == playerSide) {
                    pieces.add(piece);
                }
            }
        }
        return pieces;
    }

    private static List<Tile> createGameBoard(Builder builder) {
        Tile[] tiles = new Tile[NUM_TILES];
        for (int i = 0; i < NUM_TILES; i++) {
            tiles[i] = Tile.createTile(i, builder.boardConfig.get(i));
        }
        return Arrays.asList(tiles);
    }

    public Tile getTile(final int tileCoordinate) {
        return gameBoard.get(tileCoordinate);
    }

    public static Board createStandardBoard() {
        final Builder builder = new Builder();

        builder.setPiece(new Rook(0, BLACK));
        builder.setPiece(new Knight(1, BLACK));
        builder.setPiece(new Bishop(2, BLACK));
        builder.setPiece(new Queen(3, BLACK));
        builder.setPiece(new King(4, BLACK));
        builder.setPiece(new Bishop(5, BLACK));
        builder.setPiece(new Knight(6, BLACK));
        builder.setPiece(new Rook(7, BLACK));
        builder.setPiece(new Pawn(8, BLACK));
        builder.setPiece(new Pawn(9, BLACK));
        builder.setPiece(new Pawn(10, BLACK));
        builder.setPiece(new Pawn(11, BLACK));
        builder.setPiece(new Pawn(12, BLACK));
        builder.setPiece(new Pawn(13, BLACK));
        builder.setPiece(new Pawn(14, BLACK));
        builder.setPiece(new Pawn(15, BLACK));

        builder.setPiece(new Pawn(48, WHITE));
        builder.setPiece(new Pawn(49, WHITE));
        builder.setPiece(new Pawn(50, WHITE));
        builder.setPiece(new Pawn(51, WHITE));
        builder.setPiece(new Pawn(52, WHITE));
        builder.setPiece(new Pawn(53, WHITE));
        builder.setPiece(new Pawn(54, WHITE));
        builder.setPiece(new Pawn(55, WHITE));
        builder.setPiece(new Rook(56, WHITE));
        builder.setPiece(new Knight(57, WHITE));
        builder.setPiece(new Bishop(58, WHITE));
        builder.setPiece(new Queen(59, WHITE));
        builder.setPiece(new King(60, WHITE));
        builder.setPiece(new Bishop(61, WHITE));
        builder.setPiece(new Knight(62, WHITE));
        builder.setPiece(new Rook(63, WHITE));

        builder.setNextPlayerMoving(WHITE);
        return builder.build();
    }

    public Player getWhitePlayer() {
        return this.whitePlayer;
    }

    public Player getBlackPlayer() {
        return this.blackPlayer;
    }

    public Iterable<Move> getAllLegalMoves() {
        Collection<Move> tempCollection = new ArrayList<>();
        tempCollection.addAll(this.getBlackPlayer().getLegalMoves());
        tempCollection.addAll(this.getWhitePlayer().getLegalMoves());
        return tempCollection;
    }

    public Pawn getEnPassantPawn() {
        return enPassantPawn;
    }

    public static class Builder {

        private Map<Integer, Piece> boardConfig;
        private PlayerSide nextPlayerMoving;
        private Pawn enPassantPawn;

        public Builder() {
            this.boardConfig = new HashMap<>();
        }

        public Builder setPiece(final Piece piece) {
            this.boardConfig.put(piece.getPosition(), piece);
            return this;
        }

        public Builder setNextPlayerMoving(final PlayerSide nextPlayerMoving) {
            this.nextPlayerMoving = nextPlayerMoving;
            return this;
        }


        public Board build() {
            return new Board(this);
        }

        public void setEnPassantPawn(Pawn enPassantPawn) {
            this.enPassantPawn = enPassantPawn;
        }
    }
}
