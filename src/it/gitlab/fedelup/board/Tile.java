package it.gitlab.fedelup.board;

import it.gitlab.fedelup.pieces.Piece;

import java.util.HashMap;
import java.util.Map;

import static it.gitlab.fedelup.board.BoardUtilities.NUM_TILES;

public abstract class Tile {

    private int tileCoordinate;
    private static Map<Integer, EmptyTile> EMPTY_TILES = fillEmptyTiles();

    private static Map<Integer, EmptyTile> fillEmptyTiles() {
        final Map<Integer, EmptyTile> emptyTileMap = new HashMap<>();

        for (int i = 0; i < NUM_TILES; i++) {
            emptyTileMap.put(i, new EmptyTile(i));
        }

        return emptyTileMap;
    }

    // only way to instantiate tiles
    public static Tile createTile(final int tileCoordinate, final Piece piece) {
        if (piece != null)
            return new OccupiedTile(tileCoordinate, piece);
        return EMPTY_TILES.get(tileCoordinate);
    }


    private Tile(int tileCoordinate) {
        this.tileCoordinate = tileCoordinate;
    }

    public abstract boolean isTileOccupied();

    public abstract Piece getPiece();

    public int getTileCoordinate() {
        return this.tileCoordinate;
    }


    public static final class EmptyTile extends Tile {

        private EmptyTile(int coordinate) {
            super(coordinate);
        }

        @Override
        public boolean isTileOccupied() {
            return false;
        }

        @Override
        public Piece getPiece() {
            return null;
        }

        @Override
        public String toString() {
            return "-";
        }

    }

    public static final class OccupiedTile extends Tile {
        private Piece pieceOnTile;

        private OccupiedTile(int coordinate, Piece pieceOnTile) {
            super(coordinate);
            this.pieceOnTile = pieceOnTile;
        }

        @Override
        public boolean isTileOccupied() {
            return true;
        }

        @Override
        public Piece getPiece() {
            return pieceOnTile;
        }

        @Override
        public String toString() {
            return this.getPiece().getPiecePlayerSide().isBlack() ? getPiece().toString().toLowerCase() :
                    getPiece().toString().toUpperCase();
        }


    }

}
