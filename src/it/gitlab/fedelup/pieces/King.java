package it.gitlab.fedelup.pieces;

import it.gitlab.fedelup.PlayerSide;
import it.gitlab.fedelup.board.Board;
import it.gitlab.fedelup.board.BoardUtilities;
import it.gitlab.fedelup.board.Move;
import it.gitlab.fedelup.board.Tile;

import java.util.ArrayList;
import java.util.List;

import static it.gitlab.fedelup.board.BoardUtilities.*;
import static it.gitlab.fedelup.board.Move.MajorAttackMove;
import static it.gitlab.fedelup.board.Move.NormalMove;

public class King extends Piece {

    private final static int[] ALL_CANDIDATE_MOVES = {-9, -8, -7, -1, 1, 7, 8, 9};


    public King(int position, PlayerSide piecePlayerSide) {
        super(PieceType.KING, position, piecePlayerSide, true);
    }

    public King(int position, PlayerSide piecePlayerSide, boolean isFirstMove) {
        super(PieceType.KING, position, piecePlayerSide, isFirstMove);
    }

    @Override
    public List<Move> calculateLegalMoves(Board board) {
        List<Move> legalMoves = new ArrayList<>();
        int candidateDestinationCoordinate;

        for (int candidate : ALL_CANDIDATE_MOVES) {
            candidateDestinationCoordinate = this.getPosition() + candidate;

            if (isFirstColumnExclusion(this.getPosition(), candidateDestinationCoordinate) ||
                    isEightColumnExclusion(this.getPosition(), candidateDestinationCoordinate)) {
                continue;
            }

            if (isValidTileCoordinate(candidateDestinationCoordinate)) {
                final Tile candidateDestinationTile = board.getTile(candidateDestinationCoordinate);
                if (!candidateDestinationTile.isTileOccupied()) {
                    legalMoves.add(new NormalMove(board, this, candidateDestinationCoordinate));
                } else {
                    final Piece pieceAtDestination = candidateDestinationTile.getPiece();
                    final PlayerSide pieceAtDestinationPlayerSide = pieceAtDestination.getPiecePlayerSide();
                    // if it's other player's piece
                    if (this.getPiecePlayerSide() != pieceAtDestinationPlayerSide) {
                        legalMoves.add(new MajorAttackMove(board, this, candidateDestinationCoordinate, pieceAtDestination));
                    }
                }

            }

        }

        return legalMoves;
    }

    @Override
    public King movePiece(Move move) {
        return new King(move.getDestinationCoordinate(), move.getMovedPiece().getPiecePlayerSide());
    }

    private static boolean isFirstColumnExclusion(final int currentPosition, final int candidateOffset) {
        return FIRST_COLUMN[currentPosition] &&
                ((candidateOffset == -9) || (candidateOffset == -1) || (candidateOffset == 7));
    }

    private static boolean isEightColumnExclusion(final int currentPosition, final int candidateOffset) {
        return EIGHTH_COLUMN[currentPosition] && ((candidateOffset == -7) || (candidateOffset == 1) || (candidateOffset == 9));
    }

    @Override
    public String toString() {
        return PieceType.KING.toString();
    }
}
