package it.gitlab.fedelup.pieces;

import it.gitlab.fedelup.PlayerSide;
import it.gitlab.fedelup.board.Board;
import it.gitlab.fedelup.board.Move;

import java.util.List;
import java.util.Objects;

public abstract class Piece {

    private final int position;
    private final PlayerSide piecePlayerSide;
    protected final boolean isFirstMove;
    private final PieceType pieceType;
    private final int cachedHashCode;

    public static final int PAWN_VALUE = 100;
    public static final int BISHOP_VALUE = 300;
    public static final int KNIGHT_VALUE = 300;
    public static final int ROOK_VALUE = 500;
    public static final int QUEEN_VALUE = 900;
    public static final int KING_VALUE = 10000;


    public Piece(PieceType pieceType, int position, PlayerSide piecePlayerSide, boolean isFirstMove) {
        this.pieceType = pieceType;
        this.position = position;
        this.piecePlayerSide = piecePlayerSide;
        this.isFirstMove = isFirstMove;
        this.cachedHashCode = computeHashChode();
    }

    private int computeHashChode() {
        return Objects.hash(position, piecePlayerSide, isFirstMove, pieceType);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Piece piece = (Piece) o;
        return position == piece.position &&
                isFirstMove == piece.isFirstMove &&
                piecePlayerSide == piece.piecePlayerSide &&
                pieceType == piece.pieceType;
    }

    @Override
    public int hashCode() {
        return cachedHashCode;
    }

    public PieceType getPieceType() {
        return pieceType;
    }

    public abstract List<Move> calculateLegalMoves(final Board board);

    public abstract Piece movePiece(Move move);

    public int getPosition() {
        return position;
    }

    public PlayerSide getPiecePlayerSide() {
        return piecePlayerSide;
    }

    public boolean isFirstMove() {
        return isFirstMove;
    }

    public int getPieceValue() {
        return this.pieceType.getPieceValue();
    }


    public enum PieceType {

        PAWN("P", PAWN_VALUE) {
            @Override
            public boolean isKing() {
                return false;
            }

            @Override
            public boolean isRook() {
                return false;
            }
        },
        ROOK("R", ROOK_VALUE) {
            @Override
            public boolean isKing() {
                return false;
            }

            @Override
            public boolean isRook() {
                return true;
            }
        },
        KNIGHT("N", KNIGHT_VALUE) {
            @Override
            public boolean isKing() {
                return false;
            }

            @Override
            public boolean isRook() {
                return false;
            }
        },
        BISHOP("B", BISHOP_VALUE) {
            @Override
            public boolean isKing() {
                return false;
            }

            @Override
            public boolean isRook() {
                return false;
            }
        },
        QUEEN("Q", QUEEN_VALUE) {
            @Override
            public boolean isKing() {
                return false;
            }

            @Override
            public boolean isRook() {
                return false;
            }
        },
        KING("K", KING_VALUE) {
            @Override
            public boolean isKing() {
                return true;
            }

            @Override
            public boolean isRook() {
                return false;
            }
        };

        private String pieceName;
        private int pieceValue;

        PieceType(final String pieceName, int pieceValue) {
            this.pieceName = pieceName;
            this.pieceValue = pieceValue;
        }

        @Override
        public String toString() {
            return this.pieceName;
        }

        public abstract boolean isKing();


        public abstract boolean isRook();

        public int getPieceValue() {
            return this.pieceValue;
        }
    }

}

