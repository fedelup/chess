package it.gitlab.fedelup.pieces;

import it.gitlab.fedelup.PlayerSide;
import it.gitlab.fedelup.board.Board;
import it.gitlab.fedelup.board.BoardUtilities;
import it.gitlab.fedelup.board.Move;
import it.gitlab.fedelup.board.Tile;

import java.util.ArrayList;
import java.util.List;

import static it.gitlab.fedelup.board.BoardUtilities.NUM_TILES_PER_ROW;
import static it.gitlab.fedelup.board.BoardUtilities.isValidTileCoordinate;
import static it.gitlab.fedelup.board.Move.MajorAttackMove;
import static it.gitlab.fedelup.board.Move.NormalMove;

public class Knight extends Piece {

    private final static int[] ALL_CANDIDATE_MOVES = {
            (-(NUM_TILES_PER_ROW * 2) - 1), (-(NUM_TILES_PER_ROW * 2) + 1), (-NUM_TILES_PER_ROW - 2), (-NUM_TILES_PER_ROW + 2),
            (+NUM_TILES_PER_ROW - 2), (+NUM_TILES_PER_ROW + 2), (+(NUM_TILES_PER_ROW * 2) - 1), (+(NUM_TILES_PER_ROW * 2) + 1)};

    public Knight(int position, PlayerSide piecePlayerSide) {
        super(PieceType.KNIGHT, position, piecePlayerSide, true);
    }

    public Knight(int position, PlayerSide piecePlayerSide, boolean isFirstMove) {
        super(PieceType.KNIGHT, position, piecePlayerSide, isFirstMove);
    }


    @Override
    public List<Move> calculateLegalMoves(Board board) {

        List<Move> legalMoves = new ArrayList<>();
        int candidateDestinationCoordinate;

        for (int candidate : ALL_CANDIDATE_MOVES) {
            candidateDestinationCoordinate = this.getPosition() + candidate;
            if (isValidTileCoordinate(candidateDestinationCoordinate)) {

                if (isFirstColumnExclusion(this.getPosition(), candidate) ||
                        isSecondColumnExclusion(this.getPosition(), candidate) ||
                        isSeventhColumnExclusion(this.getPosition(), candidate) ||
                        isEighthColumnExclusion(this.getPosition(), candidate)) {
                    continue;
                }

                final Tile candidateDestinationTile = board.getTile(candidateDestinationCoordinate);
                if (!candidateDestinationTile.isTileOccupied()) {
                    legalMoves.add(new NormalMove(board, this, candidateDestinationCoordinate));
                } else {
                    final Piece pieceAtDestination = candidateDestinationTile.getPiece();
                    final PlayerSide pieceAtDestinationPlayerSide = pieceAtDestination.getPiecePlayerSide();
                    // if it's other player's piece
                    if (this.getPiecePlayerSide() != pieceAtDestinationPlayerSide) {
                        legalMoves.add(new MajorAttackMove(board, this, candidateDestinationCoordinate, pieceAtDestination));
                    }
                }
            }
        }
        return legalMoves;
    }

    @Override
    public Knight movePiece(Move move) {
        return new Knight(move.getDestinationCoordinate(), move.getMovedPiece().getPiecePlayerSide());
    }

    private static boolean isFirstColumnExclusion(final int currentPosition, final int candidateOffset) {
        return BoardUtilities.FIRST_COLUMN[currentPosition] &&
                ((candidateOffset == -17) || (candidateOffset == -10) || (candidateOffset == 6) || (candidateOffset == 15));
    }

    private static boolean isSecondColumnExclusion(final int currentPosition, final int candidateOffset) {
        return BoardUtilities.SECOND_COLUMN[currentPosition] && ((candidateOffset == -10) || (candidateOffset == 6));
    }

    private static boolean isSeventhColumnExclusion(final int currentPosition, final int candidateOffset) {
        return BoardUtilities.SEVENTH_COLUMN[currentPosition] && ((candidateOffset == -6) || (candidateOffset == 10));
    }

    private static boolean isEighthColumnExclusion(final int currentPosition, final int candidateOffset) {
        return BoardUtilities.EIGHTH_COLUMN[currentPosition] &&
                ((candidateOffset == -15) || (candidateOffset == -6) || (candidateOffset == 10) || (candidateOffset == 17));
    }

    @Override
    public String toString() {
        return PieceType.KNIGHT.toString();
    }


}
