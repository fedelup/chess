package it.gitlab.fedelup.pieces;

import it.gitlab.fedelup.PlayerSide;
import it.gitlab.fedelup.board.Board;
import it.gitlab.fedelup.board.Move;

import java.util.ArrayList;
import java.util.List;

import static it.gitlab.fedelup.board.BoardUtilities.*;
import static it.gitlab.fedelup.board.Move.*;

public class Pawn extends Piece {

    private final static int[] ALL_CANDIDATE_MOVES = {8, 16, 7, 9};

    public Pawn(int position, PlayerSide piecePlayerSide) {
        super(PieceType.PAWN, position, piecePlayerSide, true);
    }

    public Pawn(int position, PlayerSide piecePlayerSide, boolean isFirstMove) {
        super(PieceType.PAWN, position, piecePlayerSide, isFirstMove);
    }

    @Override
    public List<Move> calculateLegalMoves(Board board) {

        List<Move> legalMoves = new ArrayList<>();
        int candidateDestinationCoordinate;

        for (int candidate : ALL_CANDIDATE_MOVES) {
            candidateDestinationCoordinate = this.getPosition() + (this.getPiecePlayerSide().getDirection() * candidate);

            if (!isValidTileCoordinate(candidateDestinationCoordinate)) {
                continue;
            }

            if (candidate == 8 && !board.getTile(candidateDestinationCoordinate).isTileOccupied()) {
                if (this.getPiecePlayerSide().isPawnPromotionSquare(candidateDestinationCoordinate)) {
                    legalMoves.add(new PawnPromotion(new PawnMove(board, this, candidateDestinationCoordinate)));
                } else {
                    legalMoves.add(new PawnMove(board, this, candidateDestinationCoordinate));
                }


            } else if (candidate == 16 && this.isFirstMove() &&
                    ((SEVENTH_RANK[this.getPosition()] && this.getPiecePlayerSide().isBlack()) ||
                            (SECOND_RANK[this.getPosition()] && this.getPiecePlayerSide().isWhite()))) {
                final int behindCandidateDestinationCoordinate = this.getPosition() + (this.getPiecePlayerSide().getDirection() * 8);
                if (!board.getTile(behindCandidateDestinationCoordinate).isTileOccupied() &&
                        !board.getTile(candidateDestinationCoordinate).isTileOccupied()) {
                    legalMoves.add(new PawnJump(board, this, candidateDestinationCoordinate));
                }


            } else if (candidate == 7 &&
                    !((EIGHTH_COLUMN[this.getPosition()] && this.getPiecePlayerSide().isWhite() ||
                            (FIRST_COLUMN[this.getPosition()] && this.getPiecePlayerSide().isBlack())))) {
                if (board.getTile(candidateDestinationCoordinate).isTileOccupied()) {
                    final Piece pieceOnCandidate = board.getTile(candidateDestinationCoordinate).getPiece();
                    if (this.getPiecePlayerSide() != pieceOnCandidate.getPiecePlayerSide()) {
                        if (this.getPiecePlayerSide().isPawnPromotionSquare(candidateDestinationCoordinate)) {
                            legalMoves.add(new PawnPromotion(new PawnAttackMove(board, this, candidateDestinationCoordinate, pieceOnCandidate)));
                        } else {
                            legalMoves.add(new PawnAttackMove(board, this, candidateDestinationCoordinate, pieceOnCandidate));
                        }
                    }
                } else if (board.getEnPassantPawn() != null) {
                    if (board.getEnPassantPawn().getPosition() == (this.getPosition() + (this.getPiecePlayerSide().getOppositeDirection()))) {
                        Piece pieceOnCandidate = board.getEnPassantPawn();
                        if (pieceOnCandidate.getPiecePlayerSide() != this.getPiecePlayerSide()) {
                            legalMoves.add(new PawnEnPassantAttackMove(board, this, candidateDestinationCoordinate, pieceOnCandidate));
                        }
                    }
                }


            } else if (candidate == 9 &&
                    !((FIRST_COLUMN[this.getPosition()] && this.getPiecePlayerSide().isWhite() ||
                            (EIGHTH_COLUMN[this.getPosition()] && this.getPiecePlayerSide().isBlack())))) {
                if (board.getTile(candidateDestinationCoordinate).isTileOccupied()) {
                    final Piece pieceOnCandidate = board.getTile(candidateDestinationCoordinate).getPiece();
                    if (this.getPiecePlayerSide() != pieceOnCandidate.getPiecePlayerSide()) {
                        if (this.getPiecePlayerSide().isPawnPromotionSquare(candidateDestinationCoordinate)) {
                            legalMoves.add(new PawnPromotion(new PawnAttackMove(board, this, candidateDestinationCoordinate, pieceOnCandidate)));
                        } else {
                            legalMoves.add(new PawnAttackMove(board, this, candidateDestinationCoordinate, pieceOnCandidate));
                        }
                    }
                } else if (board.getEnPassantPawn() != null) {
                    if (board.getEnPassantPawn().getPosition() == (this.getPosition() - (this.getPiecePlayerSide().getOppositeDirection()))) {
                        Piece pieceOnCandidate = board.getEnPassantPawn();
                        if (pieceOnCandidate.getPiecePlayerSide() != this.getPiecePlayerSide()) {
                            legalMoves.add(new PawnEnPassantAttackMove(board, this, candidateDestinationCoordinate, pieceOnCandidate));
                        }
                    }
                }
            }

        }


        return legalMoves;
    }

    @Override
    public Pawn movePiece(Move move) {
        return new Pawn(move.getDestinationCoordinate(), move.getMovedPiece().getPiecePlayerSide());
    }

    @Override
    public String toString() {
        return PieceType.PAWN.toString();
    }

    public Piece getPromotionPiece() {
        return new Queen(this.getPosition(), this.getPiecePlayerSide(), false);
    }
}
