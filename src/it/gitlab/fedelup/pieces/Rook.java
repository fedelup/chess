package it.gitlab.fedelup.pieces;

import it.gitlab.fedelup.PlayerSide;
import it.gitlab.fedelup.board.Board;
import it.gitlab.fedelup.board.BoardUtilities;
import it.gitlab.fedelup.board.Move;
import it.gitlab.fedelup.board.Tile;

import java.util.ArrayList;
import java.util.List;

import static it.gitlab.fedelup.board.BoardUtilities.isValidTileCoordinate;
import static it.gitlab.fedelup.board.Move.MajorAttackMove;
import static it.gitlab.fedelup.board.Move.NormalMove;

public class Rook extends Piece {

    private final static int[] ALL_CANDIDATE_MOVES = {-8, -1, 1, 8};


    public Rook(int position, PlayerSide piecePlayerSide) {
        super(PieceType.ROOK, position, piecePlayerSide, true);
    }

    public Rook(int position, PlayerSide piecePlayerSide, boolean isFirstMove) {
        super(PieceType.ROOK, position, piecePlayerSide, isFirstMove);
    }

    @Override
    public List<Move> calculateLegalMoves(Board board) {

        List<Move> legalMoves = new ArrayList<>();
        int candidateDestinationCoordinate;

        for (int candidate : ALL_CANDIDATE_MOVES) {
            candidateDestinationCoordinate = this.getPosition();

            while (isValidTileCoordinate(candidateDestinationCoordinate)) {

                if (isFirstColumnExclusion(candidateDestinationCoordinate, candidate) ||
                        isEightColumnExclusion(candidateDestinationCoordinate, candidate)) {
                    break;
                }

                candidateDestinationCoordinate += candidate;
                if (isValidTileCoordinate(candidateDestinationCoordinate)) {

                    final Tile candidateDestinationTile = board.getTile(candidateDestinationCoordinate);
                    if (!candidateDestinationTile.isTileOccupied()) {
                        legalMoves.add(new NormalMove(board, this, candidateDestinationCoordinate));
                    } else {
                        final Piece pieceAtDestination = candidateDestinationTile.getPiece();
                        final PlayerSide pieceAtDestinationPlayerSide = pieceAtDestination.getPiecePlayerSide();
                        // if it's other player's piece
                        if (this.getPiecePlayerSide() != pieceAtDestinationPlayerSide) {
                            legalMoves.add(new MajorAttackMove(board, this, candidateDestinationCoordinate, pieceAtDestination));
                        }
                        break;
                    }
                }
            }
        }


        return legalMoves;
    }

    @Override
    public Rook movePiece(Move move) {
        return new Rook(move.getDestinationCoordinate(), move.getMovedPiece().getPiecePlayerSide());
    }


    private static boolean isFirstColumnExclusion(final int currentPosition, final int candidateOffset) {
        return BoardUtilities.FIRST_COLUMN[currentPosition] && (candidateOffset == -1);
    }

    private static boolean isEightColumnExclusion(final int currentPosition, final int candidateOffset) {
        return BoardUtilities.EIGHTH_COLUMN[currentPosition] && (candidateOffset == 1);
    }

    @Override
    public String toString() {
        return PieceType.ROOK.toString();
    }
}
