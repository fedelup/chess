package it.gitlab.fedelup;

import it.gitlab.fedelup.player.BlackPlayer;
import it.gitlab.fedelup.player.Player;
import it.gitlab.fedelup.player.WhitePlayer;

import static it.gitlab.fedelup.board.BoardUtilities.EIGHTH_RANK;
import static it.gitlab.fedelup.board.BoardUtilities.FIRST_RANK;

public enum PlayerSide {
    BLACK {
        @Override
        public boolean isPawnPromotionSquare(int position) {
            return FIRST_RANK[position];
        }

        @Override
        public int getDirection() {
            return 1;
        }

        @Override
        public int getOppositeDirection() {
            return -1;
        }

        @Override
        public boolean isWhite() {
            return false;
        }

        @Override
        public boolean isBlack() {
            return true;
        }

        @Override
        public Player choosePlayer(WhitePlayer whitePlayer, BlackPlayer blackPlayer) {
            return blackPlayer;
        }
    },
    WHITE {
        @Override
        public boolean isPawnPromotionSquare(int position) {
            return EIGHTH_RANK[position];
        }

        @Override
        public int getDirection() {
            return -1;
        }

        @Override
        public int getOppositeDirection() {
            return 1;
        }

        @Override
        public boolean isWhite() {
            return true;
        }

        @Override
        public boolean isBlack() {
            return false;
        }

        @Override
        public Player choosePlayer(WhitePlayer whitePlayer, BlackPlayer blackPlayer) {
            return whitePlayer;
        }
    };

    public abstract boolean isPawnPromotionSquare(int position);

    public abstract int getDirection();

    public abstract int getOppositeDirection();

    public abstract boolean isWhite();

    public abstract boolean isBlack();

    public abstract Player choosePlayer(WhitePlayer whitePlayer, BlackPlayer blackPlayer);

}
