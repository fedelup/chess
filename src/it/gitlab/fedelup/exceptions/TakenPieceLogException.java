package it.gitlab.fedelup.exceptions;

public class TakenPieceLogException extends RuntimeException {
    public TakenPieceLogException(String e) {
        super(e);
    }
}
