package it.gitlab.fedelup.exceptions;

public class MoveFactoryInstantiationException extends RuntimeException {
    public MoveFactoryInstantiationException(String e) {
        super(e);
    }
}
