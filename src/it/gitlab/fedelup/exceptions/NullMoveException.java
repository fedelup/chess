package it.gitlab.fedelup.exceptions;

public class NullMoveException extends RuntimeException {
    public NullMoveException(String e) {
        super(e);
    }
}
