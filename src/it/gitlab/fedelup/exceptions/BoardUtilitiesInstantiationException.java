package it.gitlab.fedelup.exceptions;

public class BoardUtilitiesInstantiationException extends RuntimeException {
    public BoardUtilitiesInstantiationException(String e) {
        super(e);
    }
}
