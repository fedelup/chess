package it.gitlab.fedelup.exceptions;

public class KingIsMissingException extends RuntimeException {
    public KingIsMissingException(String error) {
        super(error);
    }
}
