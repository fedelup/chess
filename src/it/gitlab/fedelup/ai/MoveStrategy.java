package it.gitlab.fedelup.ai;

import it.gitlab.fedelup.board.Board;
import it.gitlab.fedelup.board.Move;

public interface MoveStrategy {

    Move execute(Board board);

}
