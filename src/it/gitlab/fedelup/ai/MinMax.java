package it.gitlab.fedelup.ai;

import it.gitlab.fedelup.board.Board;
import it.gitlab.fedelup.board.Move;
import it.gitlab.fedelup.player.MoveTransition;

public class MinMax implements MoveStrategy {

    private BoardEvaluator boardEvaluator;
    private int searchDepth;

    public MinMax(int searchDepth) {
        this.boardEvaluator = new StandardBoardEvaluator();
        this.searchDepth = searchDepth;
    }

    @Override
    public String toString() {
        return "MM";
    }


    @Override
    public Move execute(Board board) {

        long startTime = System.currentTimeMillis();
        Move bestMove = null;
        int maxValue = Integer.MIN_VALUE;
        int minValue = Integer.MAX_VALUE;
        int currentVal;

        System.out.println(board.getCurrentPlayer() + " thinking, actual depth = " + this.searchDepth);
        int numMoves = board.getCurrentPlayer().getLegalMoves().size();

        for (Move move : board.getCurrentPlayer().getLegalMoves()) {
            MoveTransition moveTransition = board.getCurrentPlayer().makeMove(move);
            if (moveTransition.getMoveStatus().isDone()) {
                currentVal = board.getCurrentPlayer().getPlayerSide().isWhite() ?
                        findMin(moveTransition.getTransitionBoard(), this.searchDepth - 1) :
                        findMax(moveTransition.getTransitionBoard(), this.searchDepth - 1);

                if (board.getCurrentPlayer().getPlayerSide().isWhite() && currentVal >= maxValue) {
                    maxValue = currentVal;
                    bestMove = move;
                } else if (board.getCurrentPlayer().getPlayerSide().isBlack() && currentVal <= minValue) {
                    minValue = currentVal;
                    bestMove = move;
                }
            }
        }

        long executionTime = System.currentTimeMillis() - startTime;

        return bestMove;
    }

    public int findMin(Board board, int depth) {

        if (depth == 0 || isGameOver(board)) {
            return this.boardEvaluator.evaluate(board, depth);
        }
        int minValue = Integer.MAX_VALUE;
        for (Move move : board.getCurrentPlayer().getLegalMoves()) {
            MoveTransition moveTransition = board.getCurrentPlayer().makeMove(move);
            if (moveTransition.getMoveStatus().isDone()) {
                int currentVal = findMax(moveTransition.getTransitionBoard(), depth - 1);
                if (currentVal <= minValue) {
                    minValue = currentVal;
                }
            }
        }
        return minValue;
    }

    public int findMax(Board board, int depth) {
        if (depth == 0 || isGameOver(board)) {
            return this.boardEvaluator.evaluate(board, depth);
        }
        int maxValue = Integer.MIN_VALUE;
        for (Move move : board.getCurrentPlayer().getLegalMoves()) {
            MoveTransition moveTransition = board.getCurrentPlayer().makeMove(move);
            if (moveTransition.getMoveStatus().isDone()) {
                int currentVal = findMin(moveTransition.getTransitionBoard(), depth - 1);
                if (currentVal >= maxValue) {
                    maxValue = currentVal;
                }
            }
        }
        return maxValue;
    }

    private static boolean isGameOver(Board board) {
        return board.getCurrentPlayer().isInCheckMate() ||
                board.getCurrentPlayer().isInStaleMate();
    }
}
