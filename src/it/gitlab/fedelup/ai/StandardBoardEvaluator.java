package it.gitlab.fedelup.ai;

import it.gitlab.fedelup.board.Board;
import it.gitlab.fedelup.board.Move;
import it.gitlab.fedelup.pieces.Piece;
import it.gitlab.fedelup.player.Player;

import static it.gitlab.fedelup.board.BoardUtilities.*;

public final class StandardBoardEvaluator implements BoardEvaluator {


    private static final int CHECK_BONUS = 50;
    private static final int CHECK_MATE_BONUS = 10000;
    private static final int DEPTH_BONUS = 100;
    private static final int CASTLE_BONUS = 60;

    @Override
    public int evaluate(Board board, int depth) {
        return scorePlayer(board, board.getWhitePlayer(), depth) -
                scorePlayer(board, board.getBlackPlayer(), depth);
    }

    private int scorePlayer(Board board, Player player, int depth) {
        return getPiecesValue(player) + getMobility(player) +
                getCheck(player) + getCheckMate(player, depth) + castled(player);
    }

    private static int castled(Player player) {
        return player.isCastled() ? CASTLE_BONUS : 0;
    }

    private static int getCheckMate(Player player, int depth) {
        return player.getOpponent().isInCheckMate() ? CHECK_MATE_BONUS * depthBonus(depth) : 0;
    }

    private static int depthBonus(int depth) {
        return depth == 0 ? 1 : DEPTH_BONUS * depth;
    }

    private static int getCheck(Player player) {
        return player.getOpponent().isInCheck() ? CHECK_BONUS : 0;
    }

    private static int getMobility(Player player) {
        int mobility = 0;
        for (Move move : player.getLegalMoves()) {
            int pos = move.getCurrentCoordinate();
            if (FIRST_RANK[pos])
                mobility += 1;
            else if (SECOND_RANK[pos])
                mobility += 1;
            else if (THIRD_RANK[pos])
                mobility += 1;
            else if (FOURTH_RANK[pos])
                mobility += 1;
            else if (FIFTH_RANK[pos])
                mobility += 1;
            else if (SIXTH_RANK[pos])
                mobility += 1;
            else if (SEVENTH_RANK[pos])
                mobility += 1;
            else if (EIGHTH_RANK[pos])
                mobility += 1;
        }
        return mobility;
    }

    private static int getPiecesValue(Player player) {
        int pieceValScore = 0;
        for (Piece piece : player.getActivePieces()) {
            pieceValScore += piece.getPieceValue();
        }
        return pieceValScore;
    }
}
