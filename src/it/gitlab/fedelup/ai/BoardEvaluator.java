package it.gitlab.fedelup.ai;

import it.gitlab.fedelup.board.Board;

public interface BoardEvaluator {

    int evaluate(Board board, int depth);
}
