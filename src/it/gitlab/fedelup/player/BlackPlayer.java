package it.gitlab.fedelup.player;

import it.gitlab.fedelup.PlayerSide;
import it.gitlab.fedelup.board.Board;
import it.gitlab.fedelup.board.Move;
import it.gitlab.fedelup.board.Tile;
import it.gitlab.fedelup.pieces.Piece;
import it.gitlab.fedelup.pieces.Rook;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static it.gitlab.fedelup.board.Move.KingSideCastleMove;
import static it.gitlab.fedelup.board.Move.QueenSideCastleMove;

public class BlackPlayer extends Player {

    public BlackPlayer(Board board, Collection<Move> whiteStandardLegalMoves, Collection<Move> blackStandardLegalMoves) {
        super(board, blackStandardLegalMoves, whiteStandardLegalMoves);
    }

    @Override
    public Collection<Piece> getActivePieces() {
        return this.getBoard().getBlackPieces();
    }

    @Override
    public PlayerSide getPlayerSide() {
        return PlayerSide.BLACK;
    }

    @Override
    public Player getOpponent() {
        return this.getBoard().getWhitePlayer();
    }

    @Override
    protected Collection<Move> calculateKingCastles(Collection<Move> playerLegals, Collection<Move> opponentsLegals) {

        List<Move> kingCastles = new ArrayList<>();

        if (this.getPlayerKing().isFirstMove() && !this.isInCheck()) {
            //king side castling
            if (!this.getBoard().getTile(5).isTileOccupied() &&
                    !this.getBoard().getTile(6).isTileOccupied()) {
                Tile rookTile = this.getBoard().getTile(7);
                if (rookTile.isTileOccupied() && rookTile.getPiece().isFirstMove()) {
                    if (Player.calculateAttacksOnTile(5, opponentsLegals).isEmpty() &&
                            Player.calculateAttacksOnTile(6, opponentsLegals).isEmpty() &&
                            rookTile.getPiece().getPieceType().isRook()) {
                        kingCastles.add(new KingSideCastleMove(this.getBoard(), this.getPlayerKing(),
                                6, (Rook) rookTile.getPiece(), rookTile.getTileCoordinate(), 5));
                    }
                }
            }
            //queen side castling
            if (!this.getBoard().getTile(1).isTileOccupied() &&
                    !this.getBoard().getTile(2).isTileOccupied() &&
                    !this.getBoard().getTile(3).isTileOccupied()) {
                Tile rookTile = this.getBoard().getTile(0);
                if (rookTile.isTileOccupied() && rookTile.getPiece().isFirstMove() &&
                        Player.calculateAttacksOnTile(2, opponentsLegals).isEmpty() &&
                        Player.calculateAttacksOnTile(3, opponentsLegals).isEmpty() &&
                        rookTile.getPiece().getPieceType().isRook()) {
                    kingCastles.add(new QueenSideCastleMove(this.getBoard(), this.getPlayerKing(),
                            2, (Rook) rookTile.getPiece(), rookTile.getTileCoordinate(), 3));
                }
            }
        }
        return kingCastles;
    }
}
