package it.gitlab.fedelup.player;

import it.gitlab.fedelup.PlayerSide;
import it.gitlab.fedelup.board.Board;
import it.gitlab.fedelup.board.Move;
import it.gitlab.fedelup.board.Tile;
import it.gitlab.fedelup.pieces.Piece;
import it.gitlab.fedelup.pieces.Rook;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static it.gitlab.fedelup.board.Move.KingSideCastleMove;
import static it.gitlab.fedelup.board.Move.QueenSideCastleMove;

public class WhitePlayer extends Player {

    public WhitePlayer(Board board, Collection<Move> whiteStandardLegalMoves, Collection<Move> blackStandardLegalMoves) {
        super(board, whiteStandardLegalMoves, blackStandardLegalMoves);
    }

    @Override
    public Collection<Piece> getActivePieces() {
        return this.getBoard().getWhitePieces();
    }

    @Override
    public PlayerSide getPlayerSide() {
        return PlayerSide.WHITE;
    }

    @Override
    public Player getOpponent() {
        return this.getBoard().getBlackPlayer();
    }

    @Override
    protected Collection<Move> calculateKingCastles(Collection<Move> playerLegals, Collection<Move> opponentsLegals) {

        List<Move> kingCastles = new ArrayList<>();

        if (this.getPlayerKing().isFirstMove() && !this.isInCheck()) {
            //king side
            if (!this.getBoard().getTile(61).isTileOccupied() && !this.getBoard().getTile(62).isTileOccupied()) {
                Tile rookTile = this.getBoard().getTile(63);
                if (rookTile.isTileOccupied() && rookTile.getPiece().isFirstMove()) {
                    if (Player.calculateAttacksOnTile(61, opponentsLegals).isEmpty() &&
                            Player.calculateAttacksOnTile(62, opponentsLegals).isEmpty() &&
                            rookTile.getPiece().getPieceType().isRook()) {
                        kingCastles.add(new KingSideCastleMove(this.getBoard(), this.getPlayerKing(),
                                62, (Rook) rookTile.getPiece(), rookTile.getTileCoordinate(), 61));
                    }
                }
            }
            //queen side
            if (!this.getBoard().getTile(59).isTileOccupied() &&
                    !this.getBoard().getTile(58).isTileOccupied() &&
                    !this.getBoard().getTile(57).isTileOccupied()) {
                Tile rookTile = this.getBoard().getTile(56);
                if (rookTile.isTileOccupied() && rookTile.getPiece().isFirstMove() &&
                        Player.calculateAttacksOnTile(58, opponentsLegals).isEmpty() &&
                        Player.calculateAttacksOnTile(59, opponentsLegals).isEmpty() &&
                        rookTile.getPiece().getPieceType().isRook()) {
                    kingCastles.add(new QueenSideCastleMove(this.getBoard(), this.getPlayerKing(),
                            58, (Rook) rookTile.getPiece(), rookTile.getTileCoordinate(), 59));
                }

            }
        }
        return kingCastles;
    }
}
