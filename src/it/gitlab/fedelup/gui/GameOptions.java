package it.gitlab.fedelup.gui;

import it.gitlab.fedelup.PlayerSide;
import it.gitlab.fedelup.player.Player;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static it.gitlab.fedelup.gui.Table.PlayerType;

public class GameOptions extends JDialog {

    private PlayerType whitePlayerType;
    private PlayerType blackPlayerType;
    private JSpinner depthSpinner;

    private static final String HUMAN_PLAYER_TEXT = "Human";
    private static final String COMPUTER_PLAYER_TEXT = "Computer";

    GameOptions(JFrame frame, boolean modal) {
        super(frame, modal);
        JPanel panel = new JPanel(new GridLayout(0, 1));
        JRadioButton whiteHumanButton = new JRadioButton(HUMAN_PLAYER_TEXT);
        JRadioButton whiteComputerButton = new JRadioButton(COMPUTER_PLAYER_TEXT);
        JRadioButton blackHumanButton = new JRadioButton(HUMAN_PLAYER_TEXT);
        JRadioButton blackComputerButton = new JRadioButton(COMPUTER_PLAYER_TEXT);

        whiteHumanButton.setActionCommand(HUMAN_PLAYER_TEXT);
        ButtonGroup whiteGroup = new ButtonGroup();
        whiteGroup.add(whiteHumanButton);
        whiteGroup.add(whiteComputerButton);
        whiteHumanButton.setSelected(true);

        ButtonGroup blackGroup = new ButtonGroup();
        blackGroup.add(blackHumanButton);
        blackGroup.add(blackComputerButton);
        blackHumanButton.setSelected(true);

        getContentPane().add(panel);
        panel.add(new JLabel("White"));
        panel.add(whiteHumanButton);
        panel.add(whiteComputerButton);
        panel.add(new JLabel("Black"));
        panel.add(blackHumanButton);
        panel.add(blackComputerButton);

        panel.add(new JLabel("Search"));
        this.depthSpinner = addLabeledSpinner(panel, "Search Depth", new SpinnerNumberModel(6, 0, Integer.MAX_VALUE, 1));

        JButton cancelButton = new JButton("Cancel");
        JButton okButton = new JButton("Apply");

        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                whitePlayerType = whiteComputerButton.isSelected() ? PlayerType.COMPUTER : PlayerType.HUMAN;
                blackPlayerType = blackComputerButton.isSelected() ? PlayerType.COMPUTER : PlayerType.HUMAN;
                GameOptions.this.setVisible(false);
            }
        });

        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GameOptions.this.setVisible(false);
            }
        });

        panel.add(cancelButton);
        panel.add(okButton);

        setLocationRelativeTo(frame);
        pack();
        setVisible(false);

    }


    protected void promptUser() {
        setVisible(true);
        repaint();
    }

    protected boolean isAIPlayer(Player player) {
        if (player.getPlayerSide() == PlayerSide.WHITE) {
            return getWhitePlayerType() == PlayerType.COMPUTER;
        }
        return getBlackPlayerType() == PlayerType.COMPUTER;
    }

    private PlayerType getWhitePlayerType() {
        return this.whitePlayerType;
    }

    private PlayerType getBlackPlayerType() {
        return this.blackPlayerType;
    }

    private static JSpinner addLabeledSpinner(Container c, String label, SpinnerModel model) {
        JLabel newLabel = new JLabel(label);
        c.add(newLabel);
        JSpinner spinner = new JSpinner(model);
        newLabel.setLabelFor(spinner);
        c.add(spinner);
        return spinner;
    }

    public int getDepth() {
        return (Integer) this.depthSpinner.getValue();
    }

}
