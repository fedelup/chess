package it.gitlab.fedelup.gui;

import com.google.common.collect.Lists;
import it.gitlab.fedelup.ai.MinMax;
import it.gitlab.fedelup.ai.MoveStrategy;
import it.gitlab.fedelup.board.Board;
import it.gitlab.fedelup.board.BoardUtilities;
import it.gitlab.fedelup.board.Move;
import it.gitlab.fedelup.board.Tile;
import it.gitlab.fedelup.pieces.Piece;
import it.gitlab.fedelup.player.MoveTransition;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static it.gitlab.fedelup.board.Move.MoveFactory;
import static javax.swing.SwingUtilities.*;

public class Table extends Observable {

    private final static Dimension OUTER_FRAME_DIMENSION = new Dimension(600, 600);
    private static final Dimension BOARD_PANEL_DIMENSION = new Dimension(400, 350);
    private static final Dimension TILE_PANEL_DIMENSION = new Dimension(10, 10);
    private static final String defaultPieceImagesPath = "icons/standard/";

    private final Color lightTileColor = Color.decode("#FFFACD");
    private final Color darkTileColor = Color.decode("#593E1A");


    private final JFrame gameFrame;
    private final BoardPanel boardPanel;
    private Board chessBoard;
    private final MoveLog moveLog;
    private GameHistoryPanel gameHistoryPanel;
    private TakenPiecesPanel takenPiecesPanel;
    private GameOptions gameOptions;

    private boolean highLightLegalMoves;
    private Tile sourceTile;
    private Tile destinationTile;
    private Piece humanMovedPiece;
    private BoardDirection boardDirection;
    private Move computerMove;


    private static final Table INSTANCE = new Table();

    private Table() {
        this.gameFrame = new JFrame("Chess");
        this.gameFrame.setLayout(new BorderLayout());
        JMenuBar tableMenuBar = createTableMenuBar();
        this.gameFrame.setJMenuBar(tableMenuBar);
        this.gameFrame.setSize(OUTER_FRAME_DIMENSION);
        this.chessBoard = Board.createStandardBoard();
        this.gameHistoryPanel = new GameHistoryPanel();
        this.takenPiecesPanel = new TakenPiecesPanel();
        this.boardPanel = new BoardPanel();
        this.gameOptions = new GameOptions(this.gameFrame, true);
        this.moveLog = new MoveLog();
        this.addObserver(new TableGameAIWatcher());
        this.boardDirection = BoardDirection.NORMAL;
        this.highLightLegalMoves = true;
        this.gameFrame.add(this.takenPiecesPanel, BorderLayout.WEST);
        this.gameFrame.add(this.gameHistoryPanel, BorderLayout.EAST);
        this.gameFrame.add(this.boardPanel, BorderLayout.CENTER);
        this.gameFrame.setVisible(true);

    }

    public static Table get() {
        return INSTANCE;
    }

    public void show() {
        Table.get().getMoveLog().clear();
        Table.get().getGameHistoryPanel().redo(chessBoard, Table.get().getMoveLog());
        Table.get().getTakenPiecesPanel().redo(Table.get().getMoveLog());
        Table.get().getBoardPanel().drawBoard(Table.get().getChessBoard());
    }

    private JMenuBar createTableMenuBar() {
        JMenuBar tableMenuBar = new JMenuBar();
        tableMenuBar.add(createFileMenu());
        tableMenuBar.add(createSetingsMenu());
        tableMenuBar.add(createOptionsMEnu());

        return tableMenuBar;
    }

    private JMenu createFileMenu() {
        JMenu fileMenu = new JMenu("File");

        JMenuItem openPGN = new JMenuItem("Load PGN File");
        openPGN.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.print("PGN FILE OPENING");
            }
        });
        fileMenu.add(openPGN);

        final JMenuItem exitMenuItem = new JMenuItem("Exit");
        exitMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Runtime.getRuntime().exit(0);
            }
        });
        fileMenu.add(exitMenuItem);

        return fileMenu;
    }

    public Board getChessBoard() {
        return chessBoard;
    }


    private class BoardPanel extends JPanel {
        private List<TilePanel> boardTiles;

        BoardPanel() {
            super(new GridLayout(8, 8));
            this.boardTiles = new ArrayList<>();
            for (int i = 0; i < BoardUtilities.NUM_TILES; i++) {
                TilePanel tilePanel = new TilePanel(this, i);
                this.boardTiles.add(tilePanel);
                add(tilePanel);
            }
            setPreferredSize(BOARD_PANEL_DIMENSION);
            validate();
        }

        public void drawBoard(Board board) {
            removeAll();
            for (TilePanel tilePanel : boardDirection.traverse(boardTiles)) {
                tilePanel.drawTile(board);
                add(tilePanel);
            }
            validate();
            repaint();
        }
    }

    private JMenu createSetingsMenu() {
        JMenu settingsMenu = new JMenu("Settings");
        JMenuItem flipBoardMenuItem = new JMenuItem("Flip Board");
        flipBoardMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boardDirection = boardDirection.opposite();
                boardPanel.drawBoard(chessBoard);
            }
        });
        settingsMenu.add(flipBoardMenuItem);
        settingsMenu.addSeparator();

        JCheckBoxMenuItem legalMoveHighLighterCheckBox = new JCheckBoxMenuItem("Highlight Available Moves", true);
        legalMoveHighLighterCheckBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                highLightLegalMoves = legalMoveHighLighterCheckBox.isSelected();
            }
        });
        settingsMenu.add(legalMoveHighLighterCheckBox);

        return settingsMenu;
    }

    private JMenu createOptionsMEnu() {
        JMenu optionsMenu = new JMenu("Game Options");
        JMenuItem setupGameMenuItem = new JMenuItem("Setup");
        setupGameMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Table.get().getGameOptions().promptUser();
                Table.get().setupUpdate(Table.get().getGameOptions());
            }
        });
        optionsMenu.add(setupGameMenuItem);

        return optionsMenu;
    }

    private void setupUpdate(GameOptions gameOptions) {
        setChanged();
        notifyObservers(gameOptions);
    }

    private static class TableGameAIWatcher implements Observer {

        @Override
        public void update(Observable o, Object arg) {
            if (Table.get().getGameOptions().isAIPlayer(Table.get().getChessBoard().getCurrentPlayer()) &&
                    !Table.get().getChessBoard().getCurrentPlayer().isInCheckMate() &&
                    !Table.get().getChessBoard().getCurrentPlayer().isInStaleMate()) {
                // create AI
                // execute AI
                final AIThinkTank thinkTank = new AIThinkTank();
                thinkTank.execute();
            }
            if (Table.get().getChessBoard().getCurrentPlayer().isInCheckMate()) {
                System.out.println("Game Over, " + Table.get().getChessBoard().getCurrentPlayer() + " is in checkmate");
            }
            if (Table.get().getChessBoard().getCurrentPlayer().isInStaleMate()) {
                System.out.println("Game Over, " + Table.get().getChessBoard().getCurrentPlayer() + " is in stalemate");
            }
        }
    }

    public void updateGameBoard(Board board) {
        this.chessBoard = board;
    }

    public void updateComputerMove(Move move) {
        this.computerMove = move;
    }

    private MoveLog getMoveLog() {
        return this.moveLog;
    }

    private GameHistoryPanel getGameHistoryPanel() {
        return this.gameHistoryPanel;
    }

    private TakenPiecesPanel getTakenPiecesPanel() {
        return this.takenPiecesPanel;
    }

    private BoardPanel getBoardPanel() {
        return this.boardPanel;
    }

    private void moveMadeUpdate(PlayerType playerType) {
        setChanged();
        notifyObservers(playerType);
    }


    private static class AIThinkTank extends SwingWorker<Move, String> {

        private AIThinkTank() {

        }

        @Override
        protected Move doInBackground() throws Exception {
            MoveStrategy minMax = new MinMax(4);

            Move bestMove = minMax.execute(Table.get().getChessBoard());
            return bestMove;
        }

        @Override
        protected void done() {
            try {
                Move bestMove = get();
                Table.get().updateComputerMove(bestMove);
                Table.get().updateGameBoard(Table.get().getChessBoard().getCurrentPlayer().makeMove(bestMove).getTransitionBoard());
                Table.get().getMoveLog().addMove(bestMove);
                Table.get().getGameHistoryPanel().redo(Table.get().getChessBoard(), Table.get().getMoveLog());
                Table.get().getTakenPiecesPanel().redo(Table.get().getMoveLog());
                Table.get().getBoardPanel().drawBoard(Table.get().getChessBoard());
                Table.get().moveMadeUpdate(PlayerType.COMPUTER);

            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
    }

    private GameOptions getGameOptions() {
        return this.gameOptions;
    }


    public enum BoardDirection {
        NORMAL {
            @Override
            List<TilePanel> traverse(List<TilePanel> boardTiles) {
                return boardTiles;
            }

            @Override
            BoardDirection opposite() {
                return FLIPPED;
            }
        },
        FLIPPED {
            @Override
            List<TilePanel> traverse(List<TilePanel> boardTiles) {
                return Lists.reverse(boardTiles);
            }

            @Override
            BoardDirection opposite() {
                return NORMAL;
            }
        };

        abstract List<TilePanel> traverse(List<TilePanel> boardTiles);

        abstract BoardDirection opposite();
    }


    public static class MoveLog {

        private final List<Move> moves;

        MoveLog() {
            this.moves = new ArrayList<>();
        }

        public List<Move> getMoves() {
            return moves;
        }

        public void addMove(Move move) {
            this.moves.add(move);
        }

        public int size() {
            return this.moves.size();
        }

        public void clear() {
            this.moves.clear();
        }

        public Move removeMove(int index) {
            return this.moves.remove(index);
        }

        public boolean removeMove(Move move) {
            return this.moves.remove(move);
        }

    }

    enum PlayerType {
        HUMAN {
            @Override
            public boolean isHuman() {
                return true;
            }

            @Override
            public boolean isComputer() {
                return false;
            }
        },
        COMPUTER {
            @Override
            public boolean isHuman() {
                return false;
            }

            @Override
            public boolean isComputer() {
                return true;
            }
        };

        public abstract boolean isHuman();

        public abstract boolean isComputer();
    }

    private class TilePanel extends JPanel {

        private final int tileID;

        TilePanel(BoardPanel boardPanel, int tileID) {
            super(new GridBagLayout());
            this.tileID = tileID;
            setPreferredSize(TILE_PANEL_DIMENSION);
            assignTileColor();
            assignTilePieceIcon(chessBoard);

            addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(MouseEvent e) {

                    if (isRightMouseButton(e)) {
                        sourceTile = null;
                        destinationTile = null;
                        humanMovedPiece = null;
                    } else if (isLeftMouseButton(e)) {
                        if (sourceTile == null) {
                            sourceTile = chessBoard.getTile(tileID);
                            humanMovedPiece = sourceTile.getPiece();
                            if (humanMovedPiece == null) {
                                sourceTile = null;
                            }
                        } else {
                            destinationTile = chessBoard.getTile(tileID);
                            Move move = MoveFactory.createMove(chessBoard, sourceTile.getTileCoordinate(), destinationTile.getTileCoordinate());
                            MoveTransition transition = chessBoard.getCurrentPlayer().makeMove(move);
                            if (transition.getMoveStatus().isDone()) {
                                chessBoard = transition.getTransitionBoard();
                                moveLog.addMove(move);
                            }
                            sourceTile = null;
                            destinationTile = null;
                            humanMovedPiece = null;
                        }
                        invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                gameHistoryPanel.redo(chessBoard, moveLog);
                                takenPiecesPanel.redo(moveLog);

                                if (gameOptions.isAIPlayer(chessBoard.getCurrentPlayer())) {
                                    Table.get().moveMadeUpdate(PlayerType.HUMAN);
                                }
                                boardPanel.drawBoard(chessBoard);
                                if (Table.get().getChessBoard().getCurrentPlayer().isInCheckMate()) {
                                    System.out.println("Game Over, " + Table.get().getChessBoard().getCurrentPlayer() + " is in checkmate");
                                    JOptionPane.showMessageDialog(Table.get().getBoardPanel(), "Game Over " +
                                            Table.get().getChessBoard().getCurrentPlayer().getPlayerSide() + " is in checkmate", "Game Over",JOptionPane.INFORMATION_MESSAGE);
                                }
                                if (Table.get().getChessBoard().getCurrentPlayer().isInStaleMate()) {
                                    System.out.println("Game Over, " + Table.get().getChessBoard().getCurrentPlayer() + " is in checkmate");
                                    JOptionPane.showMessageDialog(Table.get().getBoardPanel(), "Game Over " +
                                            Table.get().getChessBoard().getCurrentPlayer().getPlayerSide() + " is in stalemate", "Game Over",JOptionPane.INFORMATION_MESSAGE);

                                }
                            }

                        });

                    }
                }

                @Override
                public void mousePressed(MouseEvent e) {
                    // no need to extend
                }

                @Override
                public void mouseReleased(MouseEvent e) {
                    // no need to extend
                }

                @Override
                public void mouseEntered(MouseEvent e) {
                    // no need to extend
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    // no need to extend
                }
            });
            validate();
        }

        private void highLightLegals(final Board board) {
            if (highLightLegalMoves) {
                for (Move move : getPieceLegalMoves(board)) {
                    if (move.getDestinationCoordinate() == this.tileID) {
                        try {
                            add(new JLabel(new ImageIcon(ImageIO.read(new File("icons/misc/green_dot.png")))));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }

        private Collection<Move> getPieceLegalMoves(Board board) {
            if (humanMovedPiece != null && humanMovedPiece.getPiecePlayerSide() == board.getCurrentPlayer().getPlayerSide()) {
                return humanMovedPiece.calculateLegalMoves(board);
            }
            return Collections.emptyList();
        }


        private void assignTilePieceIcon(Board board) {
            this.removeAll();
            if (board.getTile(tileID).isTileOccupied()) {
                try {
                    final BufferedImage image = ImageIO.read(new File(defaultPieceImagesPath +
                            board.getTile(this.tileID).getPiece().getPiecePlayerSide().toString().substring(0, 1) + board.getTile(this.tileID).getPiece().toString() + ".gif"));
                    add(new JLabel(new ImageIcon(image)));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        private void assignTileColor() {
            if (BoardUtilities.EIGHTH_RANK[this.tileID] ||
                    BoardUtilities.SIXTH_RANK[this.tileID] ||
                    BoardUtilities.FOURTH_RANK[this.tileID] ||
                    BoardUtilities.SECOND_RANK[this.tileID]) {
                if (this.tileID % 2 == 0)
                    setBackground(lightTileColor);
                else
                    setBackground(darkTileColor);
            } else if (BoardUtilities.SEVENTH_RANK[this.tileID] ||
                    BoardUtilities.FIFTH_RANK[this.tileID] ||
                    BoardUtilities.THIRD_RANK[this.tileID] ||
                    BoardUtilities.FIRST_RANK[this.tileID]) {
                if (this.tileID % 2 != 0)
                    setBackground(lightTileColor);
                else
                    setBackground(darkTileColor);
            }
        }

        public void drawTile(Board board) {
            assignTileColor();
            assignTilePieceIcon(board);
            highLightLegals(board);
            validate();
            repaint();

        }
    }

}
