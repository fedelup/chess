package it.gitlab.fedelup.gui;

import com.google.common.primitives.Ints;
import it.gitlab.fedelup.board.Move;
import it.gitlab.fedelup.exceptions.TakenPieceLogException;
import it.gitlab.fedelup.pieces.Piece;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EtchedBorder;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static it.gitlab.fedelup.gui.Table.MoveLog;

public class TakenPiecesPanel extends JPanel {


    private static final Dimension TAKE_PIECES_DIMENSION = new Dimension(40, 80);
    private static final Color PANEL_COLOR = Color.blue;
    private static final EtchedBorder PANEL_BORDER = new EtchedBorder(EtchedBorder.RAISED);


    private JPanel topPanel;
    private JPanel bottomPanel;

    public TakenPiecesPanel() {
        super(new BorderLayout());
        setBorder(PANEL_BORDER);
        this.topPanel = new JPanel(new GridLayout(8, 2));
        this.bottomPanel = new JPanel(new GridLayout(8, 2));

        this.topPanel.setBackground(PANEL_COLOR);
        this.bottomPanel.setBackground(PANEL_COLOR);

        add(this.topPanel, BorderLayout.NORTH);
        add(this.bottomPanel, BorderLayout.SOUTH);

        setPreferredSize(TAKE_PIECES_DIMENSION);
    }

    public void redo(MoveLog moveLog) {

        bottomPanel.removeAll();
        topPanel.removeAll();

        List<Piece> whiteTakenPieces = new ArrayList<>();
        List<Piece> blackTakenPieces = new ArrayList<>();

        for (Move move : moveLog.getMoves()) {
            if (move.isAttack()) {
                Piece attackedPiece = move.getAttackedPiece();
                if (attackedPiece.getPiecePlayerSide().isWhite()) {
                    whiteTakenPieces.add(attackedPiece);
                } else if (attackedPiece.getPiecePlayerSide().isBlack()) {
                    blackTakenPieces.add(attackedPiece);
                } else {
                    throw new TakenPieceLogException("Taken piece is neither black nor white");
                }
            }
        }

        Collections.sort(whiteTakenPieces, new Comparator<Piece>() {
            @Override
            public int compare(Piece o1, Piece o2) {
                return Ints.compare(o1.getPieceValue(), o2.getPieceValue());
            }
        });

        Collections.sort(blackTakenPieces, new Comparator<Piece>() {
            @Override
            public int compare(Piece o1, Piece o2) {
                return Ints.compare(o1.getPieceValue(), o2.getPieceValue());
            }
        });

        for (Piece takenPiece : whiteTakenPieces) {
            try {
                BufferedImage image = ImageIO.read(new File("icons/standard/" +
                        takenPiece.getPiecePlayerSide().toString().substring(0, 1) + "" + takenPiece.toString() + ".gif"));
                ImageIcon imageIcon = new ImageIcon(image);
                JLabel imageLabel = new JLabel(new ImageIcon(imageIcon.getImage()
                        .getScaledInstance(imageIcon.getIconWidth() - 15, imageIcon.getIconWidth() - 15, Image.SCALE_SMOOTH)));
                this.topPanel.add(imageLabel);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        for (Piece takenPiece : blackTakenPieces) {
            try {
                BufferedImage image = ImageIO.read(new File("icons/standard/" +
                        takenPiece.getPiecePlayerSide().toString().substring(0, 1) + "" + takenPiece.toString() + ".gif"));
                ImageIcon imageIcon = new ImageIcon(image);
                JLabel imageLabel = new JLabel(new ImageIcon(imageIcon.getImage()
                        .getScaledInstance(imageIcon.getIconWidth() - 15, imageIcon.getIconWidth() - 15, Image.SCALE_SMOOTH)));
                this.bottomPanel.add(imageLabel);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        validate();
    }
}
